<?php declare(strict_types=1);

use JaredF\Number;
use PHPUnit\Framework\TestCase;

final class NumberTest extends TestCase
{
    /**
     * @dataProvider providerNumber
     */
    public function testNumber(int $number): void
    {
        $number = new Number($number);
        $excepts = "$number";
        $this->assertSame($excepts, (string) $number);
    }

    /**
     * @dataProvider providerFizz
     */
    public function testFizz(int $number): void
    {
        $number = new Number($number);
        $excepts = "Fizz";
        $this->assertSame($excepts, (string) $number);
    }

    /**
     * @dataProvider providerBuzz
     */
    public function testBuzz(int $number): void
    {
        $number = new Number($number);
        $excepts = "Buzz";
        $this->assertSame($excepts, (string) $number);
    }

    /**
     * @dataProvider providerFizzBuzz
     */
    public function testFizzBuzz(int $number): void
    {
        $number = new Number($number);
        $excepts = "FizzBuzz";
        $this->assertSame($excepts, (string) $number);
    }

    public function providerNumber(){
        return [
            [1], [2], [4], [7], [8], [11], [13], [14], [43], [44], [46], [47]
        ];
    }

    public function providerFizz(){
        return [
            [3], [6], [9], [33], [36], [39], [48], [57]
        ];
    }

    public function providerBuzz(){
        return [
            [5], [10], [20], [40], [50], [80], [95]
        ];
    }

    public function providerFizzBuzz(){
        return [
            [15], [30], [45], [60], [75], [90]
        ];
    }
}