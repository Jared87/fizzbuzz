<?php
namespace JaredF;

class Number
{
    /** @var int */
    private $number;

    /**
     * @param int $number
     */
    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * Prints for multiples of three Fizz and for the multiples of five Buzz otherwise the given number.
     * @return string
     */
    public function __toString(): string
    {
        $output = '';
        if ($this->number % 3  === 0) {
            $output .= 'Fizz';
        }
        if ($this->number % 5  === 0) {
            $output .= 'Buzz';
        }
        return $output ?: $this->number;
    }
}
